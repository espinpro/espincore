#!/bin/bash

function gotest () {
    go test -coverprofile=/tmp/coverage.out -cover -timeout 30s -race -p 1 ./...; go tool cover -html=/tmp/coverage.out -o coverage.html; go tool cover -func=/tmp/coverage.out | grep total
}

function golinter () {
    golangci-lint run ./...
}

function gobench () {
  go test -bench=. ./...
}

function gobench_collect () {
  go test -bench=$1 -benchmem -count 5 | tee $2.txt
}

function gobench_compare () {
  go install golang.org/x/perf/cmd/benchstat@latest
  benchstat $1.txt $2.txt
}

function memory_analyze () {
  go build -gcflags=-m *.go 2>&1
}

function gotest_full () {
  set -o pipefail
  go test -race -tags 'test' -coverprofile=coverage.out ./... | tee -a test.out
  EXIT_STATUS=$?

  if [ $EXIT_STATUS != "0" ]; then
    echo "One of the test failed with code $EXIT_STATUS"
    exit 33
  fi
  coverageInformation="$(go tool cover -func=coverage.out)"
  [[ $coverageInformation =~ total:.*(statements).[^0-9]*([0-9]*)(\.?[0-9])(%) ]]
  currentTotalCoverage=${BASH_REMATCH[2]}
  echo ${coverageInformation}
  echo "Total coverage: ${currentTotalCoverage}%"

  cat test.out | go-junit-report -set-exit-code > report.xml

  go tool cover -html=coverage.out -o coverage.html
  go install github.com/boumenot/gocover-cobertura@latest
  "${GOPATH}"/bin/gocover-cobertura < coverage.out > coverage.xml
}