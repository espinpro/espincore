package middleware

import (
	"context"
	"log/slog"
	"testing"

	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/otel"
	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/stderr"

	"github.com/nats-io/nats.go"

	"bitbucket.org/espinpro/espincore/v3/fastlog"
	"bitbucket.org/espinpro/espincore/v3/fastlog/handlers/otel"
	"bitbucket.org/espinpro/espincore/v3/fastlog/metrics"
	"bitbucket.org/espinpro/espincore/v3/testutils"
)

func TestMetrics(t *testing.T) {
	ctx := context.Background()
	fastlog.SetupDefaultLog()
	m, err := metrics.Default(ctx)
	testutils.Equal(t, err, nil)
	defer m.Shutdown()
	defer otel.Default(ctx).Shutdown()

	mm := NewMetrics(CreateMeasures())
	err = mm.Call(func(ctx context.Context, _ *nats.Msg) error {
		slog.Default().ErrorContext(ctx, "Log message of metrics collect")
		return nil
	})(ctx, &nats.Msg{
		Subject: "test-metric",
	})

	testutils.Equal(t, err, nil)
}
