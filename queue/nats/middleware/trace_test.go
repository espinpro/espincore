package middleware

import (
	"context"
	"log/slog"
	"testing"

	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/otel"
	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/stderr"

	"github.com/nats-io/nats.go"
	"go.opentelemetry.io/otel/trace"

	"bitbucket.org/espinpro/espincore/v3/fastlog"
	"bitbucket.org/espinpro/espincore/v3/fastlog/handlers/otel"
	"bitbucket.org/espinpro/espincore/v3/fastlog/middlewares"
	"bitbucket.org/espinpro/espincore/v3/queue/nats/natsprop"
	"bitbucket.org/espinpro/espincore/v3/testutils"
)

func TestTrace(t *testing.T) {
	ctx := context.Background()
	fastlog.SetupDefaultLog(middlewares.NewGDPRMiddleware())
	defer otel.Default(ctx).Shutdown()

	sc := trace.NewSpanContext(trace.SpanContextConfig{
		TraceID: trace.TraceID{0x03},
		SpanID:  trace.SpanID{0x03},
	})
	ctx = trace.ContextWithRemoteSpanContext(ctx, sc)

	tr := NewTracer()
	err := tr.Call(func(ctx context.Context, msg *nats.Msg) error {
		t.Helper()

		slog.Default().ErrorContext(ctx, "Log message with external trace id")
		spanContext := natsprop.Extract(ctx, msg)
		testutils.Equal(t, spanContext.TraceID(), trace.TraceID{0x03})

		return nil
	})(ctx, &nats.Msg{})
	testutils.Equal(t, err, nil)
}
