package utils

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"testing"

	"bitbucket.org/espinpro/espincore/v3/multiproc/worker"
	"bitbucket.org/espinpro/espincore/v3/testutils"
)

func TestPoolExecuteSameError(t *testing.T) {
	defer func() {
		if rval := recover(); rval != nil {
			slog.Default().Error("recover", "rval", rval)
		}
	}()
	ctx := context.Background()
	err := errors.New("test error")
	pool := NewPool(ctx, worker.RecoverWrapper)

	pool.Execute(func(_ context.Context) error {
		return fmt.Errorf("error: %w", err)
	})
	pool.Execute(func(_ context.Context) error {
		return fmt.Errorf("error: %w", err)
	})
	pool.Execute(func(_ context.Context) error {
		return fmt.Errorf("error: %w", err)
	})

	poolErr := pool.Wait()
	testutils.Equal(t, poolErr, err)
	testutils.Equal(t, poolErr.Error(), "error: test error")
}

func TestPoolExecuteNoError(t *testing.T) {
	defer func() {
		if rval := recover(); rval != nil {
			slog.Default().Error("recover", "rval", rval)
		}
	}()
	ctx := context.Background()
	pool := NewPool(ctx, worker.RecoverWrapper)

	pool.Execute(func(_ context.Context) error {
		return nil
	})
	pool.Execute(func(_ context.Context) error {
		return nil
	})
	pool.Execute(func(_ context.Context) error {
		return nil
	})

	poolErr := pool.Wait()
	testutils.Equal(t, poolErr, nil)
}
