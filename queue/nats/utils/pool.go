package utils

import (
	"context"
	"sync/atomic"
	"time"
)

// Pool of workers
type Pool struct {
	ctx    context.Context
	cancel func()

	wrapper func(ctx context.Context, next func(ctx context.Context) error) error

	counter uint64
	done    uint64
	chErr   chan error
}

// NewPool return new pool
func NewPool(
	ctx context.Context,
	wrappers ...func(ctx context.Context, next func(ctx context.Context) error) error,
) *Pool {
	var wrapper func(ctx context.Context, next func(ctx context.Context) error) error
	if len(wrappers) == 1 && wrappers[0] != nil {
		wrapper = wrappers[0]
	}

	ctx, cancel := context.WithCancel(ctx)

	return &Pool{
		wrapper: wrapper,
		ctx:     ctx,
		cancel:  cancel,
		chErr:   make(chan error),
	}
}

func (p *Pool) Close() {
	p.cancel()
	close(p.chErr)
}

func (p *Pool) Size() uint64 {
	totalAdd := atomic.LoadUint64(&p.counter)
	totalDone := atomic.LoadUint64(&p.done)

	return totalAdd - totalDone
}

// Execute function in gorutine
func (p *Pool) Execute(fn func(ctx context.Context) error) {
	atomic.AddUint64(&p.counter, 1)

	go func() {
		defer func() {
			atomic.AddUint64(&p.done, 1)
		}()

		err := p.execute(fn)
		if err != nil {
			p.chErr <- err
		}
	}()
}

func (p *Pool) execute(fn func(ctx context.Context) error) error {
	if p.wrapper == nil {
		return fn(p.ctx)
	}

	return p.wrapper(p.ctx, fn)
}

// Wait wait for all functions
func (p *Pool) Wait() error {
	tk := time.NewTicker(time.Second)
	defer tk.Stop()

	for {
		select {
		case <-p.ctx.Done():
			return nil
		case err, ok := <-p.chErr:
			if !ok {
				return nil
			}

			if err != nil {
				return err
			}
		case <-tk.C:
			if p.Size() == 0 {
				return nil
			}
		}
	}
}
