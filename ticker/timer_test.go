package ticker

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/espinpro/espincore/v3/testutils"
)

func TestExecuteWithDelay(t *testing.T) {
	e := NewExecuteWithDelay()
	e.Start(context.Background(), func(_ context.Context) {}, 10*time.Second)
	testutils.Equal(t, e.IsActive(), true)
	e.Stop()
	testutils.Equal(t, e.IsActive(), false)
}
