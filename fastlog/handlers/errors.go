package handlers

import "bitbucket.org/espinpro/espincore/v3/errors"

var ErrNotFoundHandler = errors.New("not found handler")
