//go:build local_test
// +build local_test

package fastlog

import (
	"context"
	"fmt"
	"log/slog"
	"testing"
	"time"

	"go.opentelemetry.io/otel/trace"

	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/logfile"
	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/logstash"
	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/nop"
	"bitbucket.org/espinpro/espincore/v3/fastlog/handlers/otel"
	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/otel"
	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/stderr"
	_ "bitbucket.org/espinpro/espincore/v3/fastlog/handlers/stdout"
	"bitbucket.org/espinpro/espincore/v3/fastlog/middlewares"
)

func TestLog(t *testing.T) {
	SetupDefaultLog(middlewares.NewGDPRMiddleware())
	defer otel.Default(context.Background()).Shutdown()

	slog.Default().
		With(
			slog.Group("user",
				slog.String("id", "user-123"),
				slog.String("email", "user-123"),
				slog.Time("created_at", time.Now()),
			),
		).
		With("environment", "dev").
		With("password", "maxim").
		Error("A message",
			slog.String("foo", "bar"),
			slog.Any("error", fmt.Errorf("an error")))
}

func TestTrace(t *testing.T) {
	ctx := context.Background()
	SetupDefaultLog(middlewares.NewGDPRMiddleware())
	defer otel.Default(ctx).Shutdown()

	sc := trace.NewSpanContext(trace.SpanContextConfig{
		TraceID: trace.TraceID{0x03},
		SpanID:  trace.SpanID{0x03},
	})
	ctx = trace.ContextWithRemoteSpanContext(ctx, sc)

	ctx, span := otel.Default(ctx).Tracer(ctx, "default", "span-context", trace.SpanKindClient)
	slog.Default().
		ErrorContext(ctx, "New log message",
			slog.String("foo", "bar"),
			slog.Any("error", fmt.Errorf("an error")))
	slog.Default().
		ErrorContext(ctx, "New log message2",
			slog.String("foo", "bar"),
			slog.Any("error", fmt.Errorf("an error")))
	otel.Default(ctx).TracerEnd(span)

	ctx, span = otel.Default(ctx).Tracer(ctx, "default", "span-context2", trace.SpanKindClient)
	slog.Default().
		ErrorContext(ctx, "New log message3",
			slog.String("foo", "bar"),
			slog.Any("error", fmt.Errorf("an error")))
	otel.Default(ctx).TracerEnd(span)
}
