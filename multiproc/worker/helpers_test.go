package worker

import (
	"context"
	"log/slog"
	"sync/atomic"
	"testing"
	"time"

	"bitbucket.org/espinpro/espincore/v3/testutils"
)

func TestFunctionWithTimeout(t *testing.T) {
	fn := FunctionWithTimeout(time.Millisecond, func(ctx context.Context) error {
		t := time.NewTicker(time.Millisecond)
		for {
			select {
			case <-t.C:
				slog.Default().Info("new tick")
			case <-ctx.Done():
				slog.Default().Info("context finish")
				return nil
			}
		}
	})
	testutils.Equal(t, fn(context.Background()), ErrFunctionTimeout)

	fn = FunctionWithTimeout(time.Second, func(ctx context.Context) error {
		t := time.NewTimer(time.Millisecond)
		for {
			select {
			case <-t.C:
				slog.Default().Info("new tick")
				return nil
			case <-ctx.Done():
				slog.Default().Info("context finish")
				return nil
			}
		}
	})
	testutils.Equal(t, fn(context.Background()), nil)
}

func TestRunSyncMultipleWorkers(t *testing.T) {
	ctx := context.Background()
	ch := make(chan int, 100)
	var count int32
	go func(ch chan int) {
		for i := 0; i < 100; i++ {
			ch <- i
		}
		close(ch)
	}(ch)
	RunSyncMultipleWorkers(ctx, 4, func(_ context.Context) {
		for range ch {
			atomic.AddInt32(&count, 1)
		}
	})

	testutils.Equal(t, atomic.LoadInt32(&count), int32(100))
}

func TestPoolWithTimeoutFunction(t *testing.T) {
	p := NewPool(context.Background())
	p.Execute(FunctionWithTimeout(time.Millisecond, func(_ context.Context) error {
		time.Sleep(time.Second)
		return nil
	}))
	testutils.Equal(t, p.Wait(), ErrFunctionTimeout)

	p = NewPool(context.Background())
	p.Execute(func(ctx context.Context) error {
		err := FunctionWithTimeout(time.Millisecond, func(_ context.Context) error {
			time.Sleep(time.Second)
			return nil
		})(ctx)
		testutils.Equal(t, err, ErrFunctionTimeout)
		return nil
	})
	testutils.Equal(t, p.Wait(), nil)
}

func TestPoolExecuteList(t *testing.T) {
	defer func() {
		if rval := recover(); rval != nil {
			slog.Default().Error("recover", "rval", rval)
		}
	}()
	ctx := context.Background()
	pool := NewPool(ctx, RecoverWrapper)
	pool.Execute(func(_ context.Context) error {
		panic(ErrPanic)
	})
	poolErr := pool.Wait()
	testutils.Equal(t, poolErr, ErrPanic)
}

func TestRunAsyncMultipleWorkers(t *testing.T) {
	ctx := context.Background()
	in := make(chan int, 100)
	go func() {
		for i := 0; i < 100; i++ {
			in <- i
		}
		close(in)
	}()
	out := RunAsyncMultipleWorkers(ctx, 4, 100, func(_ context.Context, ch chan<- interface{}) {
		for v := range in {
			ch <- v
		}
	})
	var count int32
	for range out {
		atomic.AddInt32(&count, 1)
	}
	testutils.Equal(t, count, atomic.LoadInt32(&count))
}

func TestFanInOut(t *testing.T) {
	ctx := context.Background()
	in := make(chan interface{}, 100)
	go func() {
		for i := 0; i < 1; i++ {
			in <- i
		}
		close(in)
	}()
	var c int32
	FanInOut(ctx, 4, 100, in, func(_ context.Context, _ interface{}) {
		atomic.AddInt32(&c, 1)
	})
	testutils.Equal(t, atomic.LoadInt32(&c), int32(4))
}
