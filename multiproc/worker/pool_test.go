package worker

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"testing"

	"bitbucket.org/espinpro/espincore/v3/testutils"
)

func TestPoolExecuteSameError(t *testing.T) {
	defer func() {
		if rval := recover(); rval != nil {
			slog.Default().Error("recover", "rval", rval)
		}
	}()
	ctx := context.Background()
	err := errors.New("test error")
	pool := NewPool(ctx, RecoverWrapper)

	pool.Execute(func(_ context.Context) error {
		return fmt.Errorf("error: %w", err)
	})
	pool.Execute(func(_ context.Context) error {
		return fmt.Errorf("error: %w", err)
	})
	pool.Execute(func(_ context.Context) error {
		return fmt.Errorf("error: %w", err)
	})

	poolErr := pool.Wait()
	testutils.Equal(t, poolErr, err)
	testutils.Equal(t, poolErr.Error(), "error: test error")
}

func TestPoolExecuteNoError(t *testing.T) {
	defer func() {
		if rval := recover(); rval != nil {
			slog.Default().Error("recover", "rval", rval)
		}
	}()
	ctx := context.Background()
	pool := NewPool(ctx, RecoverWrapper)

	pool.Execute(func(_ context.Context) error {
		return nil
	})
	pool.Execute(func(_ context.Context) error {
		return nil
	})
	pool.Execute(func(_ context.Context) error {
		return nil
	})

	poolErr := pool.Wait()
	testutils.Equal(t, poolErr, nil)
}

func TestPool(t *testing.T) {
	err := errors.New("test")
	err2 := errors.New("test2")
	err3 := errors.New("test3")
	ctx := context.Background()
	p := NewPool(ctx)
	p.Execute(func(context.Context) error {
		return nil
	})
	p.Execute(func(context.Context) error {
		return nil
	})
	p.Execute(func(context.Context) error {
		return err
	})
	resErr := p.Wait()
	testutils.Equal(t, errors.Is(resErr, err), true)
	testutils.Equal(t, errors.Is(resErr, err2), false)
	testutils.Equal(t, errors.Is(resErr, err3), false)
}
