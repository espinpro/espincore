package instance

import (
	"log"

	"bitbucket.org/espinpro/espincore/v3/utils"
)

var (
	id      = utils.GetUniqueID()
	shortID string
)

func init() {
	sid, err := utils.GetShortID()
	if err != nil {
		log.Fatalf("Error getting short id: %v", err)
	}

	shortID = string(sid)
}

// GetInstanceID return current instance id
func GetInstanceID() string {
	return id
}

// GetShortInstanceID return current short instance id
func GetShortInstanceID() string {
	return shortID
}
